
variable "isPROD" {
  type    = bool
  default = false
}


variable "Environment" {
  type    = list(any)
  default = ["prod", "dev"]

}